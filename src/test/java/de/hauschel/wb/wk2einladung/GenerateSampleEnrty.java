package de.hauschel.wb.wk2einladung;


import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class GenerateSampleEnrty {

    @Test
    public void generate() throws JsonProcessingException 
    {
        EinladungsFormular einladungsFormular = new EinladungsFormular(); 
        
        Person person = new Person();
        person.setEmail("naturzukunft@hauschel.de)");
        person.setNachname("Mustermnann");
        person.setTelefonFest("08977665");
        person.setTelefonMobil("017977886");
        person.setVorname("Fredy");        
        einladungsFormular.setPerson(person);
        
        MehrUeberMich mehrUeberMich = new MehrUeberMich();
        mehrUeberMich.setVertretenePersonenkreise("vertretenePersonenkreise");
        mehrUeberMich.setVorwegMitteilung("vorwegMitteilung");
        einladungsFormular.setMehrUeberMich(mehrUeberMich);
        
        Organisation organisation = new Organisation();
        organisation.setBeschreibung("beschreibung");
        organisation.setFelder("felder");
        organisation.setGegruendet("gegruendet");
        organisation.setMeineFunktionInDerOrganisation("meineFunktionInDerOrganisation");
        organisation.setOrganisation("organisation");
        organisation.setPersonal("personal");
        organisation.setReichweite("reichweite");
        organisation.setVernetzung("vernetzung");
        organisation.setWebsite("website");
        einladungsFormular.setOrganisation(organisation);
        
        einladungsFormular.setMovitationUndWuenscheFuerKonferenz("MovitationUndWuenscheFuerKonferenz");
        
        UnterkunftVerpflegung unterkunftVerpflegung = new UnterkunftVerpflegung();
        unterkunftVerpflegung.setEssen("essen");
        unterkunftVerpflegung.setKommentar("kommentar");
        unterkunftVerpflegung.setZimmer("zimmer");
        einladungsFormular.setUnterkunftVerpflegung(unterkunftVerpflegung);
        
        Anreise anreise = new Anreise();
        anreise.setAnkunftBahn("ankunftBahn");
        anreise.setArt("art");
        anreise.setAutoMitfahrer("autoMitfahrer");        
        anreise.setKommentar("kommentar");
        einladungsFormular.setAnreise(anreise);
        
        einladungsFormular.setFinanzStatus("FinanzStatus");
                
        Rechtliches rechtliches = new Rechtliches();
        rechtliches.setDatenschutz("datenschutz");
        rechtliches.setOffentlicheNennung("offentlicheNennung");
        einladungsFormular.setRechtliches(rechtliches);

        einladungsFormular.setFeedbackZumFragebogen("FeedbackZumFragebogen");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String str = new String( objectMapper.writeValueAsString(einladungsFormular));
        System.out.println(str);
    }

}
