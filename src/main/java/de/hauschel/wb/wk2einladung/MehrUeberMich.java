package de.hauschel.wb.wk2einladung;

public class MehrUeberMich {
    private String vertretenePersonenkreise;
    private String vorwegMitteilung;

    public String getVorwegMitteilung() {
        return vorwegMitteilung;
    }

    public void setVorwegMitteilung(String vorwegMitteilung) {
        this.vorwegMitteilung = vorwegMitteilung;
    }

    public String getVertretenePersonenkreise() {
        return vertretenePersonenkreise;
    }

    public void setVertretenePersonenkreise(String vertretenePersonenkreise) {
        this.vertretenePersonenkreise = vertretenePersonenkreise;
    }

}
