package de.hauschel.wb.wk2einladung;

public class EinladungsFormular {

    private Person person;
    private MehrUeberMich mehrUeberMich;
    private Organisation organisation;
    private String movitationUndWuenscheFuerKonferenz;
    private UnterkunftVerpflegung unterkunftVerpflegung;
    private Anreise anreise;
    private String finanzStatus;
    private Rechtliches rechtliches;
    private String feedbackZumFragebogen;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public MehrUeberMich getMehrUeberMich() {
        return mehrUeberMich;
    }

    public void setMehrUeberMich(MehrUeberMich mehrUeberMich) {
        this.mehrUeberMich = mehrUeberMich;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getMovitationUndWuenscheFuerKonferenz() {
        return movitationUndWuenscheFuerKonferenz;
    }

    public void setMovitationUndWuenscheFuerKonferenz(String movitationUndWuenscheFuerKonferenz) {
        this.movitationUndWuenscheFuerKonferenz = movitationUndWuenscheFuerKonferenz;
    }

    public UnterkunftVerpflegung getUnterkunftVerpflegung() {
        return unterkunftVerpflegung;
    }

    public void setUnterkunftVerpflegung(UnterkunftVerpflegung unterkunftVerpflegung) {
        this.unterkunftVerpflegung = unterkunftVerpflegung;
    }

    public Anreise getAnreise() {
        return anreise;
    }

    public void setAnreise(Anreise anreise) {
        this.anreise = anreise;
    }

    public String getFinanzStatus() {
        return finanzStatus;
    }

    public void setFinanzStatus(String finanzStatus) {
        this.finanzStatus = finanzStatus;
    }

    public Rechtliches getRechtliches() {
        return rechtliches;
    }

    public void setRechtliches(Rechtliches rechtliches) {
        this.rechtliches = rechtliches;
    }

    public String getFeedbackZumFragebogen() {
        return feedbackZumFragebogen;
    }

    public void setFeedbackZumFragebogen(String feedbackZumFragebogen) {
        this.feedbackZumFragebogen = feedbackZumFragebogen;
    }

}
