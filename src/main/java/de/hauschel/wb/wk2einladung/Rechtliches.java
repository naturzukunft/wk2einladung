package de.hauschel.wb.wk2einladung;

public class Rechtliches {
    private String offentlicheNennung;
    private String datenschutz;

    public String getOffentlicheNennung() {
        return offentlicheNennung;
    }

    public void setOffentlicheNennung(String offentlicheNennung) {
        this.offentlicheNennung = offentlicheNennung;
    }

    public String getDatenschutz() {
        return datenschutz;
    }

    public void setDatenschutz(String datenschutz) {
        this.datenschutz = datenschutz;
    }

}
