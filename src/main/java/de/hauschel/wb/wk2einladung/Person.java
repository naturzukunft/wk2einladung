package de.hauschel.wb.wk2einladung;

public class Person {
    private String vorname;
    private String nachname;
    private String email;
    private String telefonFest;
    private String telefonMobil;

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefonFest() {
        return telefonFest;
    }

    public void setTelefonFest(String telefonFest) {
        this.telefonFest = telefonFest;
    }

    public String getTelefonMobil() {
        return telefonMobil;
    }

    public void setTelefonMobil(String telefonMobil) {
        this.telefonMobil = telefonMobil;
    }

}
