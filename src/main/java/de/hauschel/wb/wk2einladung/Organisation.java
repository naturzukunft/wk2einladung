package de.hauschel.wb.wk2einladung;

public class Organisation {
    private String organisation;
    private String website;
    private String meineFunktionInDerOrganisation;
    private String gegruendet;
    private String personal;
    private String felder;
    private String beschreibung;
    private String vernetzung;
    private String reichweite;

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMeineFunktionInDerOrganisation() {
        return meineFunktionInDerOrganisation;
    }

    public void setMeineFunktionInDerOrganisation(String meineFunktionInDerOrganisation) {
        this.meineFunktionInDerOrganisation = meineFunktionInDerOrganisation;
    }

    public String getGegruendet() {
        return gegruendet;
    }

    public void setGegruendet(String gegruendet) {
        this.gegruendet = gegruendet;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getFelder() {
        return felder;
    }

    public void setFelder(String felder) {
        this.felder = felder;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getVernetzung() {
        return vernetzung;
    }

    public void setVernetzung(String vernetzung) {
        this.vernetzung = vernetzung;
    }

    public String getReichweite() {
        return reichweite;
    }

    public void setReichweite(String reichweite) {
        this.reichweite = reichweite;
    }

}
