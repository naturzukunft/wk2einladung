package de.hauschel.wb.wk2einladung;

public class Anreise {
    private String art;
    private String kommentar;
    private String ankunftBahn;
    private String autoMitfahrer;

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public String getAnkunftBahn() {
        return ankunftBahn;
    }

    public void setAnkunftBahn(String ankunftBahn) {
        this.ankunftBahn = ankunftBahn;
    }

    public String getAutoMitfahrer() {
        return autoMitfahrer;
    }

    public void setAutoMitfahrer(String autoMitfahrer) {
        this.autoMitfahrer = autoMitfahrer;
    }

}
