package de.hauschel.wb.wk2einladung;

public class UnterkunftVerpflegung {
    private String zimmer;
    private String essen;
    private String kommentar;

    public String getZimmer() {
        return zimmer;
    }

    public void setZimmer(String zimmer) {
        this.zimmer = zimmer;
    }

    public String getEssen() {
        return essen;
    }

    public void setEssen(String essen) {
        this.essen = essen;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }
}
