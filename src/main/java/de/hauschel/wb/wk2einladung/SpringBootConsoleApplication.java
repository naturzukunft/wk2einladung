package de.hauschel.wb.wk2einladung;

import java.util.List;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.mail.ImapMailReceiver;

import lombok.extern.slf4j.Slf4j;


@SpringBootApplication
@Slf4j
public class SpringBootConsoleApplication 
  implements CommandLineRunner,ApplicationRunner {
 
    public static void main(String[] args) {
        log.info("STARTING THE APPLICATION");
        SpringApplication.run(SpringBootConsoleApplication.class, args);
        log.info("APPLICATION FINISHED");
    }
  
    @Override
    public void run(String... args) {
        log.info("EXECUTING : command line runner");
  
        for (int i = 0; i < args.length; ++i) {
            log.info("args[{}]: {}", i, args[i]);
        }
    }
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("# NonOptionArgs: " + args.getNonOptionArgs().size());

        System.out.println("NonOptionArgs:");
        args.getNonOptionArgs().forEach(System.out::println);

        System.out.println("# OptionArgs: " + args.getOptionNames().size());
        System.out.println("OptionArgs:");
        
//        List<String> pwd = args.getOptionValues("password");
//        ImapMailReceiver receiver = new ImapMailReceiver("imaps://hauschel:"+pwd.get(0)+"@imap.gmail.com:993/INBOX");
//      
//        Object[] mails = receiver.receive();
//        for (Object object : mails) {
//            System.out.println(object.getClass().getName());
//        }        
        
        args.getOptionNames().forEach(optionName -> {
            System.out.println(optionName + "=" + args.getOptionValues(optionName));
        });
    }
}